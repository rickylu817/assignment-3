import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class HigherUpTest {
    public static void main(String[] args) {
        System.out.println("Test 1.1: Num of Bifun (5) < Num of Args (6)");
        List<Double> test1Input1 = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test1BiFun1 = Arrays.asList(HigherOrderUtils.add, HigherOrderUtils.divide,
                HigherOrderUtils.multiply, HigherOrderUtils.subtract, HigherOrderUtils.add);
        System.out.println(HigherOrderUtils.zip(test1Input1,test1BiFun1) + "\n");

        System.out.println("Test 1.2: Num of Bifun (3) < Num of Args (6)");
        List<Double> test1Input2 = Arrays.asList(3.2, 4.0, 8.0, 34.0, 44.0, 123.0);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test1BiFun2 = Arrays.asList(HigherOrderUtils.divide,
                HigherOrderUtils.multiply, HigherOrderUtils.add);
        System.out.println(HigherOrderUtils.zip(test1Input2,test1BiFun2) + "\n");

        System.out.println("Test 2.1: Num of Bifun = Num of Args");
        List<Double> test2Input1 = Arrays.asList(32.0, 23.3, 2.33, 5.66, 3.23);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test2BiFun1 = Arrays.asList(HigherOrderUtils.add, HigherOrderUtils.divide,
                HigherOrderUtils.multiply, HigherOrderUtils.subtract, HigherOrderUtils.add);
        System.out.println(HigherOrderUtils.zip(test2Input1,test2BiFun1) + "\n");

        System.out.println("Test 2.2: Num of Bifun = Num of Args");
        List<Double> test2Input2 = Arrays.asList(123.0, 10.0, 33.22, 1344.2323, 56.444, 34.23233);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test2BiFun2 = Arrays.asList(HigherOrderUtils.subtract, HigherOrderUtils.divide,
                HigherOrderUtils.add, HigherOrderUtils.multiply, HigherOrderUtils.divide);
        System.out.println(HigherOrderUtils.zip(test2Input2,test2BiFun2) + "\n");

        System.out.println("Test 2.1: Num of Bifun (5) > Num of Args (3)");
        List<Double> test3Input1 = Arrays.asList(999.23232, 455.232, 33.33);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test3BiFun1 = Arrays.asList(HigherOrderUtils.divide, HigherOrderUtils.divide,
                HigherOrderUtils.multiply, HigherOrderUtils.subtract, HigherOrderUtils.add);
        System.out.println(HigherOrderUtils.zip(test3Input1,test3BiFun1) + "\n");

        System.out.println("Test 2.1: Num of Bifun (15) > Num of Args (11)");
        List<Double> test3Input2 = Arrays.asList(999.23232, 455.232, 33.33, 34343.34, 36555.33, 44.444, 123.3, 0.44, -1.23
        -4.5, -888.34);
        List<HigherOrderUtils.NamedBiFunction<Double, Double, Double>> test3BiFun2 = Arrays.asList(HigherOrderUtils.divide, HigherOrderUtils.divide,
                HigherOrderUtils.multiply, HigherOrderUtils.divide, HigherOrderUtils.subtract, HigherOrderUtils.add,
                HigherOrderUtils.divide, HigherOrderUtils.add, HigherOrderUtils.multiply, HigherOrderUtils.subtract,
                HigherOrderUtils.subtract, HigherOrderUtils.multiply, HigherOrderUtils.divide, HigherOrderUtils.divide,
                HigherOrderUtils.add);
        System.out.println(HigherOrderUtils.zip(test3Input2,test3BiFun2) + "\n");

        Function<String, Integer> f2 = s -> s.length() * 32323+ 1234;
        Function<Integer, Double> g2 = i -> i % 12333.0 ;

        Function<String, Double> fuck = new HigherOrderUtils.FunctionComposition<String, Integer, Double>().composition.apply(f2, g2);
        System.out.println("Test Composition 1");
        System.out.println(fuck.apply("I hate SBU") + "\n");
        System.out.println("Test Composition 2");
        System.out.println(fuck.apply("Papa Banerjee!"));

    }
}
