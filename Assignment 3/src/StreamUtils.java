import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUtils
{
    public static void main(String [] args)
    {
        //Problem 1
        List<String> str1 = new ArrayList<String>();
        str1.add("hello");
        str1.add("zebra");
        str1.add("apple");
        str1.add("a");
        str1.add("b");
        
        System.out.println(capitalized(str1));
        
        ArrayList<String> str2 = new ArrayList<>();
        str2.add("");
        str2.add("String");
        str2.add("yeet");
        str2.add("Up");
        str2.add("Water bottle");
        str2.add("recycle");
        str2.add("aAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        str2.add("aAAAAAAAAAAAAAAAAAAAAAAAAAABBB");
        str2.add("aAAAAAAAAAAAAAAAAAAAAAAAAAABBA");
        str2.add("aAAAAAAAAAAAAAAAAAAAAAAAAAAAAC");
        
        System.out.println(capitalized(str2));
        
        List<String> str3 = new ArrayList<String>();
        str3.add("Hello World");
        str3.add("1");
        str3.add("2");
        str3.add("none");
        System.out.println(capitalized(str3));
        
        List<String> str4 = new ArrayList<String>();
        str4.add("1");
        str4.add("2");
        str4.add("5");
        
        System.out.println(capitalized(str4));
        
        List<String> str5 = new ArrayList<String>();
        
        System.out.println(capitalized(str5));
        
        //Problem 2
        List<String> str6 = new ArrayList<String>();
        str6.add("Titan");
        str6.add("titan");
        str6.add("pe");
        str6.add("gym");
        str6.add("elon");
        
        System.out.println(longest(str6,true));
        System.out.println(longest(str6,false));
        
        List<String> str7 = new ArrayList<String>();
        str7.add("Watch");
        str7.add("Elongest");
        str7.add("Numbers");
        str7.add("Aleph");
        
        System.out.println(longest(str7,true));
        System.out.println(longest(str7,false));
        
        List<String> str8 = new ArrayList<String>();
        str8.add("s");
        str8.add("o");
        str8.add("u");
        str8.add("p");
        
        System.out.println(longest(str8,true));
        System.out.println(longest(str8,false));
        
        List<String> str9 = new ArrayList<String>();
        str9.add("s");
        str9.add("");
        str9.add("");
        str9.add("p");
        
        System.out.println(longest(str9,true));
        System.out.println(longest(str9,false));
        
        //Problem 3
        List<Integer> intlist1 = new ArrayList<Integer>();
        intlist1.add(1);
        intlist1.add(2);
        intlist1.add(0);
        
        System.out.println(least(intlist1,true));
        System.out.println(least(intlist1,false));
        
        /**
        List<Sphere> slist1 = new ArrayList<Sphere>();
        slist1.add(new Sphere(1,2,3,4));
        slist1.add(new Sphere(3,3,3,99));
        slist1.add(new Sphere(3,4,1,4));
        
        System.out.println(least(slist1,true));
        System.out.println(least(slist1,false));
        **/
        
        //Problem 4
        HashMap<String, Integer> hm1 = new HashMap<>();
        hm1.put("Hello World",1);
        hm1.put("Oh no",3);
        hm1.put("Darkness",2);
        hm1.put("Kill",5);
        
        System.out.println(flatten(hm1));
        
        //HashMap<Integer,Sphere> hm2 = new HashMap<>();
        //hm2.put(1, new Sphere(1,1,1,3));
        //hm2.put(2, new Sphere(1,1,11,4));
        
        //System.out.println(flatten(hm2));
        
        HashMap<Integer,Integer> hm3 = new HashMap<>();
        hm3.put(1,12);
        hm3.put(2,88);
        hm3.put(9, Integer.MIN_VALUE);
        hm3.put(4,444);
        
        System.out.println("Using flatten");
        System.out.println(flatten(hm3));
        System.out.println("Using entrySet");
        System.out.println(hm3.entrySet());
        
        /**
        System.out.println(longest(s,true));
        System.out.println(longest(s,false));
        System.out.println("aAAAAAAAAAAAAAAAAAAAAAAAAAAAAC".length());
        
        ArrayList<Integer> i = new ArrayList<>();
        i.add(1);
        i.add(-29);
        i.add(-29);
        i.add(24);
        i.add(0);
        System.out.println(least(i,false));
        
        HashMap<Integer,String> h = new HashMap<>();
        h.put(1, "A");
        h.put(2, "B");
        h.put(3, "C");
        
        System.out.println(flatten(h));
        **/
        
    }
    
    /**
     * @param strings: the input collection of <code>String</code>s.
     * @return a collection of those <code>String</code>s in the input
     * collection that start with a capital letter.
     */
    public static Collection<String> capitalized(Collection<String> strings)
    {
        //We use filter first to take out the empty String then call filter again
        //to eliminate those element that didn't have capital letter in the front
        return strings.stream().filter(e -> e != null).filter(e -> !e.isEmpty()).filter(e -> e.charAt(0)>=65 && e.charAt(0)<=90).collect(Collectors.toList());
    }
    
    /**
    * Find and return the longest <code>String</code> in a given collection of <code>String</code>s.
    *
    * @param strings: the given collection of <code>String</code>s.
    * @param from_start: a <code>boolean</code> flag that decides how ties are broken.
    * If <code>true</code>, then the element encountered earlier in
    * the iteration is returned, otherwise the later element is returned.
    * @return the longest <code>String</code> in the given collection,
    * where ties are broken based on <code>from_start</code>.
    */
    public static String longest(Collection<String> strings, boolean from_start)
    {
        /**
        return strings.stream().reduce("",(answer,rest) -> {
            if(answer.length()>rest.length()) return rest;
            else if(answer.length()<rest.length()) return rest;
            else return rest;
            
        });
        **/
        
        //The identity element is an empty string that will be keeping track of
        //the longest String
        return strings.stream().filter(e -> e != null).reduce("",(answer,rest)->{
            if(answer.length()<rest.length()) return rest;
            else if(answer.length()==rest.length())
            {
                if(from_start) return answer;
                else return rest;
            }
            else return answer;
        });
    }
    
    /**
     * Find and return the least element from a collection of given elements
     * that are comparable.
     *
     * @param items: the given collection of elements
     * @param from_start: a <code>boolean</code> flag that decides how ties are
     * broken. If <code>true</code>, the element encountered earlier in the
     * iteration is returned, otherwise the later element is returned.
     * @param <T>: the type parameter of the collection (i.e., the items are all
     * of type T).
     * @return the least element in <code>items</code>, where ties are broken
     * based on <code>from_start</code>.
     */
    public static <T extends Comparable<T>> T least(Collection<T> items, boolean from_start)
    {
        //This one is very similar to longest exception we use compareTo to compare
        //And since there is no identity element we have to use orElse to return a null
        //If there is no element in the items
        return items.stream().filter(e -> e != null).reduce((answer,rest) ->{
           if(answer.compareTo(rest)>0) return rest;
           else if(answer.compareTo(rest)==0)
           {
               if(from_start) return answer;
               else return rest;
           }
           else return answer;
        }).orElse(null);
    }
    
    /**
     * Flattens a map to a stream of <code>String</code>s, where each element in
     * the list is formatted as "key -> value".
     *
     * @param aMap the specified input map.
     * @param <K> the type parameter of keys in <code>aMap</code>.
     * @param <V> the type parameter of values in <code>aMap</code>.
     * @return the flattened list representation of <code>aMap</code>.
     */
    public static <K, V> List<String> flatten(Map<K, V> aMap)
    {
        //We call the keySet method on aMap to convert it into a stream and map
        //each key we get into a String that combine the key with the value
        return aMap.keySet().stream().map(k -> k+" -> "+aMap.get(k)).collect(Collectors.toList());
    }
}