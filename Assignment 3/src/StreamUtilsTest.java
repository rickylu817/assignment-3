
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StreamUtilsTest
{
    public static void main(String [] args)
    {
        //Problem 1
        List<String> str1 = Arrays.asList("Hello","null",null,"kill myself","123","%^$@$");
        StreamUtils.capitalized(str1).forEach(System.out::println);
        System.out.println("-----");
        List<String> str2 = Arrays.asList("Hello","Hello","Hello","Triple hello","triple");
        StreamUtils.capitalized(str2).forEach(System.out::println);
        System.out.println("-----");
        List<String> str3 = Arrays.asList("BIG CAPITALIZED"," Capitalized");
        StreamUtils.capitalized(str3).forEach(System.out::println);
        System.out.println("-----");
        List<String> str4 = Arrays.asList(null,null, "none"," shall","escape");
        StreamUtils.capitalized(str4).forEach(System.out::println);
        System.out.println("-----------------");
        
        //Problem 2
        List<String> str5 = Arrays.asList("BIG CAPITALIZED"," Capitalized","oh no");
        System.out.println(StreamUtils.longest(str5, true));
        System.out.println(StreamUtils.longest(str5, false));
        System.out.println("-----");
        List<String> str6 = Arrays.asList("a","b","c","d","e","f","gg","ezzz","haha");
        System.out.println(StreamUtils.longest(str6, true));
        System.out.println(StreamUtils.longest(str6, false));
        System.out.println("-----");
        List<String> str7 = Arrays.asList("ggez clap","get rekted","ha","lol","omg","ba","d");
        System.out.println(StreamUtils.longest(str7, true));
        System.out.println(StreamUtils.longest(str7, false));
        List<String> str8 = Arrays.asList("null",null,"nullspace","nope","kill","HITATO","okkokokokkookok");
        System.out.println(StreamUtils.longest(str8, true));
        System.out.println(StreamUtils.longest(str8, false));
        List<String> str9 = Arrays.asList(null,null);
        System.out.println(StreamUtils.longest(str9, true));
        System.out.println(StreamUtils.longest(str9, false));
        List<String> str10 = Arrays.asList("no way to hell","down to hell");
        System.out.println(StreamUtils.longest(str10, true));
        System.out.println(StreamUtils.longest(str10, false));
        System.out.println("-------------------------");
        //Problem 3
        List<AdditionalInt> int1 = new ArrayList<>();
        int1.add(new AdditionalInt(1,"hello"));
        int1.add(new AdditionalInt(1,"deez nuts"));
        int1.add(new AdditionalInt(2,"omg"));
        int1.add(null);
        System.out.println(StreamUtils.least(int1, true));
        System.out.println(StreamUtils.least(int1, false));
        
        List<AdditionalInt> int2 = new ArrayList<>();
        int2.add(new AdditionalInt(3,null));
        int2.add(null);
        int2.add(null);
        System.out.println(StreamUtils.least(int2, true));
        System.out.println(StreamUtils.least(int2, false));
        
        List<AdditionalInt> int3 = new ArrayList<>();
        int3.add(new AdditionalInt(1,"omg1"));
        int3.add(new AdditionalInt(1,"kill yourself"));
        int3.add(new AdditionalInt(1,"omg2"));
        int3.add(new AdditionalInt(-1,"omg3"));
        int3.add(new AdditionalInt(-4,"omg4"));
        int3.add(new AdditionalInt(-3,"kill yourself"));
        System.out.println(StreamUtils.least(int3, true));
        System.out.println(StreamUtils.least(int3, false));
        System.out.println("======= ALPHA");
        //Problem 4
        Map<Integer,String> map1 = new HashMap();
        map1.put(1, "Hello WOrld");
        map1.put(null, null);
        map1.put(2,"kill yourself");
        map1.put(3,"i want to die");
        StreamUtils.flatten(map1).stream().forEach(System.out::println);
        System.out.println("----");
        Map<String, Integer> map2 = new HashMap();
        map2.put("Kill yourself", Integer.MIN_VALUE);
        map2.put("LeAGUE OF LEGEND1", 2);
        map2.put("LeAGUE OF LEGEND2", 4);
        map2.put("LeAGUE OF LEGEND3", 3);
        map2.put("LeAGUE OF LEGEND4", 5);
        map2.put("LeAGUE OF LEGEND5", 6);
        map2.put("LeAGUE OF LEGEND6", 7);
        map2.put("LeAGUE OF LEGEND7", 8);
        StreamUtils.flatten(map2).stream().forEach(System.out::println);
        
    }
}