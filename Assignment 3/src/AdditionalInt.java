public class AdditionalInt implements Comparable<AdditionalInt>
{
    //Private variables
    private String name;
    private Integer num;
    
    public AdditionalInt(int givenInt, String name)
    {
        num = givenInt;
        this.name = name;
    }

    @Override
    public int compareTo(AdditionalInt o)
    {
        //This number is greater than the given number thus return 1
        if(num.compareTo(o.num)>0)
        {
            return 1;
        }
        else if(num.compareTo(o.num)<0)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    
    public String toString()
    {
        return "Name is "+name+", "+num;
    }
    
    public int getInteger()
    {
        return num;
    }
    
    public int getStringLength()
    {
        return name.length();
    }
}