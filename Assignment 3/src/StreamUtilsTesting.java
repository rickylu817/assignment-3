import java.util.*;
public class StreamUtilsTesting {
    public static void main(String[] args) {
        System.out.println("Capitalized Testing \n");
        System.out.println("No capitalized strings");
        Collection<String> noCapStrings = Arrays.asList("apples", "oranges", "pear", "banana", "grape", "dragonfruit",
                "watermelon", "strawberry", "peach", "lemon", "lime");
        Collection<String> noCapStringsResult = StreamUtils.capitalized(noCapStrings);
        System.out.println("No capitalized strings result: " + noCapStringsResult.isEmpty());
        System.out.println("\nHas Capitalized and Null Elements");
        Collection<String> capAndNull = Arrays.asList("Apples", "Oranges", null, "Pear", "Banana", "Grape", "Dragonfruit",
                "Watermelon", "Strawberry", null, "peach", "lemon", "lime");
        Collection<String> capAndNullResult = StreamUtils.capitalized(capAndNull);
        System.out.println("Has Capitalized and Null result");
        for (String s : capAndNullResult) {
            System.out.println(s);
        }

        System.out.println("\nHas capitalized, null, and empty");
        Collection<String> capNullEmpty = Arrays.asList("Apples", "", "Oranges", null, "Pear", "Banana", "Grape", "Dragonfruit",
                "Watermelon", "", "Strawberry", null, "peach", "lemon", "lime");
        Collection<String> capNullEmptyResult = StreamUtils.capitalized(capNullEmpty);
        System.out.println("Has Capitalized and Null result");
        for (String s : capNullEmptyResult) {
            System.out.println(s);
        }

        System.out.println("\nEmpty Collection");
        Collection<String> capitalizedEmpty = new ArrayList<>();
        Collection<String> capitalizedEmptyResult = StreamUtils.capitalized(capitalizedEmpty);
        System.out.println("Empty Collection result - is Empty " + capitalizedEmptyResult.isEmpty());

        System.out.println("\nNull Collection");
        Collection<String> capitalizedNull = Arrays.asList(null, null);
        Collection<String> capitalizedNullResult = StreamUtils.capitalized(capitalizedNull);
        System.out.println("Null Collection result");
        System.out.println("Empty Collection result - is Empty " + capitalizedNullResult.isEmpty());

        System.out.println("\nLongest Testing \n");

        System.out.println("Flag is True");
        Collection<String> trueFlag = Arrays.asList("apples", "oranges", "pear", "banana", "grape",
                "watermelon", "strawberry", "peach", "lemon", "lime");
        System.out.println("Flag is True Result: " + StreamUtils.longest(trueFlag, true));

        System.out.println("\nFlag is False");
        Collection<String> falseFlag = Arrays.asList("apples", "oranges", "pear", "banana", "grape",
                "watermelon", "peach", "lemon", "lime", "strawberry");
        System.out.println("Flag is False Result: " + StreamUtils.longest(falseFlag, false));

        System.out.println("\nEmpty Collection");
        Collection<String> longestEmptyCollection = new ArrayList<>();
        System.out.println("Empty Collection Result: " + StreamUtils.longest(longestEmptyCollection, false));

        System.out.println("\nNull Collection");
        Collection<String> longestNullCollection = Arrays.asList(null, null, null);
        System.out.println("Null Collection Result: " + StreamUtils.longest(longestNullCollection, false));

        System.out.println("\nNull, Valid, and Empty Strings");
        Collection<String> longestNull = Arrays.asList(null, "oranges", "", "pear", "banana", "grape",
                "watermelon", null, "peach", "", "lemon", "lime", "strawberry");
        System.out.println("Null, Valid, and Empty Strings Result: " + StreamUtils.longest(longestNull, false));

        System.out.println("\nLeast Testing \n");
        System.out.println("\nEmpty Collection");
        Collection<Integer> leastEmptyCollection = new ArrayList<>();
        System.out.println("Empty Collection Result: " + StreamUtils.least(leastEmptyCollection, false));

        System.out.println("\nNull Collection");
        Collection<Integer> leastNullCollection = Arrays.asList(null, null, null);
        System.out.println("Null Collection Result: " + StreamUtils.least(leastNullCollection, false));

        System.out.println("Collection with null and non null Strings - Flag is True");
        Collection<String> leastNullNonNullTrueFlag = Arrays.asList("apples", "oranges", "pear", "banana", "grape",
                "watermelon", "peach", "lemon", "lime", "strawberry");
        System.out.println("Collection with null and non null Strings - Flag is True: " + StreamUtils.least(leastNullNonNullTrueFlag, true));

        System.out.println("Collection with null and non null Strings - Flag is True");
        Collection<String> leastNullNonNullFalseFlag = Arrays.asList("apples", null, "oranges", "pear", "banana", "grape",
                "watermelon", "peach", null, "lemon", "lime", "strawberry");
        System.out.println("Collection with null and non null Strings - Flag is False Result: " + StreamUtils.least(leastNullNonNullFalseFlag, false));

        System.out.println("\nString Collection With Empty String");
        Collection<String> leastEmptyString = Arrays.asList("apples", "", "oranges", "pear", "banana", "grape",
                "watermelon", "peach", null, "lemon", "lime", "strawberry");
        System.out.println("String Collection with Empty String Result: " + StreamUtils.least(leastEmptyString, false));

        System.out.println("\nInteger Collection");
        Collection<Integer> leastIntegerCollection = Arrays.asList(88, 23, 33, 22, 1);
        System.out.println("Integer Collection Result: " + StreamUtils.least(leastIntegerCollection, false));

        System.out.println("\nCharacter Collection");
        Collection<Character> leastCharCollection = Arrays.asList('a', 'A', '?', '_', 'Z', 'f');
        System.out.println("Char Collection Result: " + StreamUtils.least(leastCharCollection, false));

        System.out.println("\nMap Testing\n");
        System.out.println("\nEmpty Map");
        Map<String, Integer> emptyMap = new HashMap<>();
        System.out.println("Empty Map Result: is Empty " + emptyMap.isEmpty());

        System.out.println("\nNull Map");
        Map<String, Integer> nullMap = new HashMap<>();
        nullMap.put(null, null);
        nullMap.put(null, null);
        System.out.println("Null Map Result");
        List<String> nullMapResult = StreamUtils.flatten(nullMap);

        System.out.println("\nMap with Non Null Elements");
        Map<String, Integer> nonNullMap = new HashMap<>();
        nonNullMap.put("Freshman", 1);
        nonNullMap.put("Sophomore", 2);
        nonNullMap.put("Junior", 3);
        nonNullMap.put("Senior", 4);
        System.out.println("Non Null Map Result");
        List<String> nonNullMapResult = StreamUtils.flatten(nonNullMap);
        for (String s : nonNullMapResult) {
            System.out.println(s);
        }

        System.out.println("\nMap with Non Null Elements and Null Key(s)");
        Map<String, Integer> nonNullNullKeyMap = new HashMap<>();
        nonNullNullKeyMap.put("Freshman", 1);
        nonNullNullKeyMap.put("Sophomore", 2);
        nonNullNullKeyMap.put(null, 3);
        nonNullNullKeyMap.put("Senior", 4);
        System.out.println("Non Null Map Result");
        List<String> nonNullNullKeyMapResult = StreamUtils.flatten(nonNullNullKeyMap);
        for (String s : nonNullNullKeyMapResult) {
            System.out.println(s);
        }

        System.out.println("\nMap with Non Null Elements and Null Value(s)");
        Map<String, Integer> nonNullNullValueMap = new HashMap<>();
        nonNullNullValueMap.put("Freshman", 1);
        nonNullNullValueMap.put("Sophomore", 2);
        nonNullNullValueMap.put("Junior", null);
        nonNullNullValueMap.put("Senior", 4);
        System.out.println("Non Null Map Result");
        List<String> nonNullNullValueMapResult = StreamUtils.flatten(nonNullNullValueMap);
        for (String s : nonNullNullValueMapResult) {
            System.out.println(s);
        }


    }
}
