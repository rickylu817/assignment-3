import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class HigherOrderUtils
{
    interface NamedBiFunction<T,U,R> extends java.util.function.BiFunction<T,U,R>
    {
        String name();
    }
    
    static class FunctionComposition<T,U,P>
    {
        public BiFunction<Function<T,U>,Function<U,P>,Function<T,P>> composition = (first,second) -> first.andThen(second);
    }
    
    //This static variable will be holding the add BiFunction
    public static NamedBiFunction<Double,Double,Double> add = new NamedBiFunction<Double,Double,Double>() {
            @Override
            public Double apply(Double d1, Double d2)
            {
                BigDecimal input1 = BigDecimal.valueOf(d1);
                BigDecimal input2 = BigDecimal.valueOf(d2);
                
                BigDecimal answer = input1.add(input2);
                
                //Use BigDecimal for accuracy
                return answer.doubleValue();
            }

            @Override
            public String name()
            {
                return "add";
            }
        };
    
    //This static variable will be holding the subtract BiFunction
    public static NamedBiFunction<Double,Double,Double> subtract = new NamedBiFunction<Double,Double,Double>() {
            @Override
            public Double apply(Double d1, Double d2)
            {
                BigDecimal input1 = BigDecimal.valueOf(d1);
                BigDecimal input2 = BigDecimal.valueOf(d2);
                
                BigDecimal answer = input1.subtract(input2);
                
                //We use BiDecimal for accuracy
                return answer.doubleValue();
            }

            @Override
            public String name()
            {
                return "diff";
            }
        };
    
    //This static variable will be holding the multiply BiFunction
    public static NamedBiFunction<Double,Double,Double> multiply = new NamedBiFunction<Double,Double,Double>() {
            @Override
            public Double apply(Double d1, Double d2)
            {
                BigDecimal input1 = BigDecimal.valueOf(d1);
                BigDecimal input2 = BigDecimal.valueOf(d2);
                
                BigDecimal answer = input1.multiply(input2);
                
                //Use BigDecimal for accuracy
                return answer.doubleValue();
            }

            @Override
            public String name()
            {
                return "mult";
            }
        };
    
    //This static variable will be holding the multiply BiFunction
    public static NamedBiFunction<Double,Double,Double> divide = new NamedBiFunction<Double,Double,Double>() {
            @Override
            public Double apply(Double d1, Double d2) throws java.lang.ArithmeticException
            {
                //This means that the divisor is a 0 which can't occur thus throw exception
                if(d2==0)
                {
                    throw new java.lang.ArithmeticException();
                }
                
                //If we are out here then the number is alright therefore we can divide
                BigDecimal input1 = BigDecimal.valueOf(d1);
                BigDecimal input2 = BigDecimal.valueOf(d2);
                
                BigDecimal answer = input1.divide(input2, MathContext.DECIMAL128);
                
                //Use BigDecimal for precision
                return answer.doubleValue();
            }

            @Override
            public String name()
            {
                return "div";
            }
        };
    
    public static void main(String [] args)
    {
        //Testing for the variables and the methods
        List<Double> nums = new ArrayList<>();
        List<NamedBiFunction<Double,Double,Double>> bi = new ArrayList<>();
        nums.add(1d);
        nums.add(2d);
        nums.add(5d);
        nums.add(4d);
        
        //bi.add(divide);
        
        Function<Character,String> f = a -> {
            String output = "";
            
            int charNum = a.charValue();
            
            for(int i=0;i<charNum;i++)
            {
                output+=a.toString();
            }
            
            return output;
        };
        
        Function<String,Integer> g = a-> {
            Integer output = 0;
            
            return a.length();
        };
        
        Function<Character,Integer> fg = new HigherOrderUtils.FunctionComposition<Character,String,Integer>().composition.apply(f, g);
        System.out.println(fg.apply('/'));
        
        bi.add(add);
        bi.add(divide);
        bi.add(multiply);
        
        System.out.println(zip(nums,bi));
        
    }
    
    /**
     * Applies a given list of bifunctions -- functions that take two arguments
     * of a certain type and produce a single instance of that type -- to a list
     * of arguments of that type. The functions are applied in an iterative
     * manner, and the result of each function is stored in the list in an
     * iterative manner as well, to be used by the next bifunction in the next
     * iteration. For example, given List<Double> args = Arrays.asList(1d, 1d,
     * 3d, 0d, 4d), and List<NamedBiFunction<Double, Double, Double>> bfs =
     * [add, multiply, add, divide], <code>zip(args, bfs)</code> will proceed
     * iteratively as follows: - index 0: the result of add(1,1) is stored in
     * args[1] to yield args = [1,2,3,0,4] - index 1: the result of
     * multiply(2,3) is stored in args[2] to yield args = [1,2,6,0,4] - index 2:
     * the result of add(6,0) is stored in args[3] to yield args = [1,2,6,6,4] -
     * index 3: the result of divide(6,4) is stored in args[4] to yield args =
     * [1,2,6,6,1]
     *
     * @param args: the arguments over which <code>bifunctions</code> will be
     * applied.
     * @param bifunctions: the list of bifunctions that will be applied on
     * <code>args</code>.
     * @param <T>: the type parameter of the arguments (e.g., Integer, Double)
     * @return the item in the last index of <code>args</code>, which has the
     * final result of all the bifunctions being applied in sequence.
     */
    public static <T> T zip(List<T> args, List<NamedBiFunction<T, T, T>> bifunctions)
    {
        //Getting the size of both List
        int argSize = args.size();
        int funSize = bifunctions.size();
        
        //More argument than functions that's fine use the normal way
        if(argSize > funSize)
        {
            //We loop over the bifunctions and do the operation accordingly to the position
            //of each bifunctionsx
            for(int i=0;i<bifunctions.size();i++)
            {
                args.set(i+1, bifunctions.get(i).apply(args.get(i), args.get(i+1)));
            }
            
            //Then the bifunction's size will tell us where the answer is located
            return args.get(bifunctions.size());
        }
        //That means either argSize and funSize have the same amount of elements
        //Or funSize have more, then we have to account for it respectively
        else
        {
            //For these two cases, we only go up to argSize - 1 times
            for(int i=0;i<argSize-1;i++)
            {
                args.set(i+1, bifunctions.get(i).apply(args.get(i), args.get(i+1)));
            }
            
            return args.get(argSize-1);
        }
    }
}