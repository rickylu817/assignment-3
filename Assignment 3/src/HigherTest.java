import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class HigherTest
{
    public static void main(String[] args) 
    {
        List<Double> int1 = Arrays.asList(1d,2d,3d,4d,5d,6d,7d,3d,2d,1d);
        List<HigherOrderUtils.NamedBiFunction<Double,Double,Double>> funs = new ArrayList<>();
        funs.add(HigherOrderUtils.add);
        funs.add(HigherOrderUtils.divide);
        funs.add(HigherOrderUtils.multiply);
        funs.add(HigherOrderUtils.subtract);
        funs.add(HigherOrderUtils.subtract);
        funs.add(HigherOrderUtils.subtract);
        funs.add(HigherOrderUtils.subtract);
        funs.add(HigherOrderUtils.divide);
        funs.add(HigherOrderUtils.multiply);

        
        System.out.println(HigherOrderUtils.zip(int1, funs));
        
        Function<AdditionalInt, Integer> f = value1 -> value1.getInteger()*2;
        Function<Integer, String> g = value1 -> {
            String output = "";
            
            for(int i=0;i<value1;i++)
            {
                output += "k";
            }
            
            return output;
        };
        
        Function<AdditionalInt, String> fg = new HigherOrderUtils.FunctionComposition<AdditionalInt, Integer, String>().composition.apply(f, g);
        
        System.out.println(fg.apply(new AdditionalInt(5,"Hello World")));
        
        
        
    }
}